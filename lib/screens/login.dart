import 'package:flutter/material.dart';
import './home.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _PageState createState() => _PageState();
}

class _PageState extends State<LoginScreen> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    var divHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Container(
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                image: DecorationImage(
                  image: AssetImage('assets/images/background-head_1.png'),
                  fit: BoxFit.cover
                )
              ),
              child: Padding(
                padding: EdgeInsets.only(top: 8.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(top: 15.0),
                        child: Center(
                          child: Text(
                            widget.title,
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.white,
                              fontWeight: FontWeight.bold
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              height: divHeight/2*0.3,
            ),

            Container(
              margin: const EdgeInsets.only(left: 20.0, right: 20.0, top: 30.0),
              child: 
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container (
                      margin: const EdgeInsets.only(bottom: 25.0),
                      child: 
                        Image.asset(
                          'assets/images/logo_anime.png'
                        ),
                    ),
                    Container (
                      margin: const EdgeInsets.only(bottom: 10.0),
                      child: 
                        TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          autofocus: false,
                          initialValue: '',
                          decoration: InputDecoration(
                            hintText: 'Email',
                            contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                          ),
                        ),
                    ),
                    
                    TextFormField(
                      autofocus: false,
                      initialValue: '',
                      obscureText: true,
                      decoration: InputDecoration(
                        hintText: 'Password',
                        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                      ),
                    ),
                    FlatButton(
                      child: Text(
                        'Forgot password?',
                        style: TextStyle(color: Colors.black54),
                      ),
                      onPressed: () {},
                    ),

                    
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 0.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(24),
                              ),
                              onPressed: () {
                                Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(builder: (context) => HomeScreen(title: 'Home')),
                                );
                              },
                              padding: EdgeInsets.all(12),
                              color: Color(0xFF61d5c5),
                              child: Text('Log In', style: TextStyle(color: Colors.white)),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container (
                      margin: const EdgeInsets.only(top: 30.0, bottom: 5.0),
                      child: 
                        Text('Login With Social'),
                    ),

                    

                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 16.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(24),
                              ),
                              onPressed: () {
                                Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(builder: (context) => HomeScreen(title: 'Home')),
                                );
                              },
                              padding: EdgeInsets.all(12),
                              color: Color(0xFF4263A2),
                              child: Text('Facebook', style: TextStyle(color: Colors.white)),
                            ),
                          ),
                          Expanded(
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(24),
                              ),
                              onPressed: () {
                                Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(builder: (context) => HomeScreen(title: 'Home')),
                                );
                              },
                              padding: EdgeInsets.all(12),
                              color: Color(0XFFD8523C),
                              child: Text('Google', style: TextStyle(color: Colors.white)),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ]
                ),
            ),

          ],
        ),
      ),
    );
  }
}
