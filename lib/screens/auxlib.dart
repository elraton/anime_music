import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import './listsongs.dart';
import './listgenres.dart';
import './listartist.dart';

class AuxScreen extends StatelessWidget {
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 0.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(24),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => ListGenresScreen(title: 'Generos')),
                        );
                      },
                      padding: EdgeInsets.all(12),
                      color: Color(0xFFA4d5fe),
                      highlightColor: Color(0xFFA4d5fe),
                      splashColor: Color(0xFFA4d5fe),
                      elevation: 4.0,
                      child: Text('Generos', style: TextStyle(color: Colors.white)),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(24),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => ListArtistScreen(title: 'Artistas')),
                        );
                      },
                      padding: EdgeInsets.all(12),
                      color: Color(0xFFA4d5fe),
                      highlightColor: Color(0xFFA4d5fe),
                      splashColor: Color(0xFFA4d5fe),
                      elevation: 4.0,
                      child: Text('Artistas', style: TextStyle(color: Colors.white)),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(right: 0.0),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(24),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => ListSongScreen(title: 'Favoritos')),
                        );
                      },
                      padding: EdgeInsets.all(12),
                      color: Color(0xFFA4d5fe),
                      highlightColor: Color(0xFFA4d5fe),
                      splashColor: Color(0xFFA4d5fe),
                      elevation: 4.0,
                      child: Text('Favoritos', style: TextStyle(color: Colors.white)),
                    ),
                  ),
                ),
              ],
            ),
          ),

          Container (
            margin: const EdgeInsets.only(top: 25.0),
            child: Row(
              children: <Widget>[
                Text(
                  'Popular',
                  style: TextStyle(
                    color: Color(0xFFA3B4FD),
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold
                  ),
                ),
                Spacer(),
                Text(
                  'todos',
                  style: TextStyle(
                    color: Color(0xFFA0EEEE),
                    fontSize: 15.0,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ],
            ),
          ),

          Container (
            margin: EdgeInsets.only(top: 25.0),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: 130.0,
                          height: 130.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Colors.grey,
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/cover1.jpg'),
                              width: 130.0,
                              height: 130.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15.0),
                          child: Text('Album1'),
                        )
                      ],
                    )
                  ),

                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: 130.0,
                          height: 130.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Colors.grey,
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/cover2.jpg'),
                              width: 130.0,
                              height: 130.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15.0),
                          child: Text('Album2'),
                        )
                      ],
                    )
                  ),

                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: 130.0,
                          height: 130.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Colors.grey,
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/cover3.jpg'),
                              width: 130.0,
                              height: 130.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15.0),
                          child: Text('Album3'),
                        )
                      ],
                    )
                  ),

                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: 130.0,
                          height: 130.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Colors.grey,
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/cover4.png'),
                              width: 130.0,
                              height: 130.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15.0),
                          child: Text('Album4'),
                        )
                      ],
                    )
                  ),
                ],
              ),
            ),
          ),

          Container (
            margin: EdgeInsets.only(top: 25.0),
            child: Row(
              children: <Widget>[
                Text(
                  'Nuevos',
                  style: TextStyle(
                    color: Color(0xFFA3B4FD),
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold
                  ),
                ),
                Spacer(),
                Text(
                  'todos',
                  style: TextStyle(
                    color: Color(0xFFA0EEEE),
                    fontSize: 15.0,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ],
            ),
          ),

          Container (
            margin: EdgeInsets.only(top: 25.0),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: 130.0,
                          height: 130.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Colors.grey,
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/cover1.jpg'),
                              width: 130.0,
                              height: 130.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15.0),
                          child: Text('Album1'),
                        )
                      ],
                    )
                  ),

                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: 130.0,
                          height: 130.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Colors.grey,
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/cover2.jpg'),
                              width: 130.0,
                              height: 130.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15.0),
                          child: Text('Album2'),
                        )
                      ],
                    )
                  ),

                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: 130.0,
                          height: 130.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Colors.grey,
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/cover3.jpg'),
                              width: 130.0,
                              height: 130.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15.0),
                          child: Text('Album3'),
                        )
                      ],
                    )
                  ),

                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: 130.0,
                          height: 130.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Colors.grey,
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/cover4.png'),
                              width: 130.0,
                              height: 130.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15.0),
                          child: Text('Album4'),
                        )
                      ],
                    )
                  ),
                ],
              ),
            ),
          ),


          Container (
            margin: EdgeInsets.only(top: 25.0),
            child: Row(
              children: <Widget>[
                Text(
                  'Recientes',
                  style: TextStyle(
                    color: Color(0xFFA3B4FD),
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold
                  ),
                ),
                Spacer(),
                Text(
                  'todos',
                  style: TextStyle(
                    color: Color(0xFFA0EEEE),
                    fontSize: 15.0,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ],
            ),
          ),

          Container (
            margin: EdgeInsets.only(top: 25.0),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: 130.0,
                          height: 130.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Colors.grey,
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/cover1.jpg'),
                              width: 130.0,
                              height: 130.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15.0),
                          child: Text('Album1'),
                        )
                      ],
                    )
                  ),

                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: 130.0,
                          height: 130.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Colors.grey,
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/cover2.jpg'),
                              width: 130.0,
                              height: 130.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15.0),
                          child: Text('Album2'),
                        )
                      ],
                    )
                  ),

                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: 130.0,
                          height: 130.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Colors.grey,
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/cover3.jpg'),
                              width: 130.0,
                              height: 130.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15.0),
                          child: Text('Album3'),
                        )
                      ],
                    )
                  ),

                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: 130.0,
                          height: 130.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Colors.grey,
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/cover4.png'),
                              width: 130.0,
                              height: 130.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15.0),
                          child: Text('Album4'),
                        )
                      ],
                    )
                  ),
                ],
              ),
            ),
          ),


          Container (
            margin: EdgeInsets.only(top: 25.0),
            child: Row(
              children: <Widget>[
                Text(
                  'Recientes',
                  style: TextStyle(
                    color: Color(0xFFA3B4FD),
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold
                  ),
                ),
                Spacer(),
                Text(
                  'todos',
                  style: TextStyle(
                    color: Color(0xFFA0EEEE),
                    fontSize: 15.0,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ],
            ),
          ),

          Container (
            margin: EdgeInsets.only(top: 25.0),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: 130.0,
                          height: 130.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Colors.grey,
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/cover1.jpg'),
                              width: 130.0,
                              height: 130.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15.0),
                          child: Text('Album1'),
                        )
                      ],
                    )
                  ),

                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: 130.0,
                          height: 130.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Colors.grey,
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/cover2.jpg'),
                              width: 130.0,
                              height: 130.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15.0),
                          child: Text('Album2'),
                        )
                      ],
                    )
                  ),

                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: 130.0,
                          height: 130.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Colors.grey,
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/cover3.jpg'),
                              width: 130.0,
                              height: 130.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15.0),
                          child: Text('Album3'),
                        )
                      ],
                    )
                  ),

                  Container(
                    margin: EdgeInsets.only(right: 10.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: 130.0,
                          height: 130.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Colors.grey,
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/cover4.png'),
                              width: 130.0,
                              height: 130.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 15.0),
                          child: Text('Album4'),
                        )
                      ],
                    )
                  ),
                ],
              ),
            ),
          ),


        ]
      );
  }
}
