import 'package:flutter/material.dart';

class CustomDrawer extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    var divHeight = MediaQuery.of(context).size.height;
    // TODO: implement build
    return ListView(
          shrinkWrap: true,
          children: <Widget>[
            Container(
              height: divHeight,
              decoration: BoxDecoration(
                color: Colors.white,
                image: DecorationImage(
                  image: AssetImage('assets/images/backmenu_drawer.png'),
                  fit: BoxFit.fitWidth,
                  alignment: Alignment(0, -1)
                )
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 15.0),
                    child: Row(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 10.0, top: 15.0),
                          alignment: AlignmentDirectional(-1, -1),
                          width: 80.0,
                          height: 80.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Colors.grey,
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: NetworkImage('http://i.pravatar.cc/300'),
                              width: 80.0,
                              height: 80.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 15.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'JHOSIMAR MAMANI',
                                style: TextStyle(
                                  color: Color(0xFFF9F9F9),
                                ),
                              ),
                              Text(
                                'elratonmaton@gmail.com',
                                style: TextStyle(
                                  color: Color(0xFFF9F9F9)
                                ),
                              ),
                            ],
                          )
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 15.0,top: 10.0),
                    child: Row(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 10.0, top: 15.0, right: 20.0),
                          alignment: AlignmentDirectional(-1, -1),
                          width: 50.0,
                          height: 50.0,
                          decoration: BoxDecoration(
                            color: Color(0xFFb597ef),
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Color(0xFFc3a6f9),
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/pulse3.png'),
                              width: 50.0,
                              height: 50.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Text(
                          'Pedido de canciones',
                          style: TextStyle(
                            color: Color(0xFFF9F9F9),
                            fontWeight: FontWeight.bold
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 15.0,top: 10.0),
                    child: Row(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 10.0, top: 15.0, right: 20.0),
                          alignment: AlignmentDirectional(-1, -1),
                          width: 50.0,
                          height: 50.0,
                          decoration: BoxDecoration(
                            color: Color(0xFFb597ef),
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Color(0xFFe9e9e9),
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/equalizer.png'),
                              width: 50.0,
                              height: 50.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Text(
                          'Ecualizador',
                          style: TextStyle(
                            color: Color(0xFFA3B4FD),
                            fontWeight: FontWeight.bold
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 15.0,top: 10.0),
                    child: Row(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 10.0, top: 15.0, right: 20.0),
                          alignment: AlignmentDirectional(-1, -1),
                          width: 50.0,
                          height: 50.0,
                          decoration: BoxDecoration(
                            color: Color(0xFFb597ef),
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Color(0xFFe9e9e9),
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/paint.png'),
                              width: 50.0,
                              height: 50.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Text(
                          'Temas',
                          style: TextStyle(
                            color: Color(0xFFA3B4FD),
                            fontWeight: FontWeight.bold
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 15.0,top: 10.0),
                    child: Row(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 10.0, top: 15.0, right: 20.0),
                          alignment: AlignmentDirectional(-1, -1),
                          width: 50.0,
                          height: 50.0,
                          decoration: BoxDecoration(
                            color: Color(0xFFb597ef),
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Color(0xFFe9e9e9),
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/share.png'),
                              width: 50.0,
                              height: 50.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Text(
                          'Comparte la aplicación',
                          style: TextStyle(
                            color: Color(0xFFA3B4FD),
                            fontWeight: FontWeight.bold
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        );
  }
}