import 'package:flutter/material.dart';
import './auxlib.dart';

class ListSongScreen extends StatefulWidget {
  ListSongScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _PageState createState() => _PageState();
}

class _PageState extends State<ListSongScreen> {
  int _counter = 0;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    var divHeight = MediaQuery.of(context).size.height;
    var divWidth = MediaQuery.of(context).size.width;

    var children = <Widget>[];
    var counter = 1;
    for (var i = 0; i < 10; i++) {
      children.add(Container(
        margin: EdgeInsets.only(bottom: 3.0),
        child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: (divWidth / 2) - 2,
                        height: 200.0,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            new BoxShadow(
                              color: Colors.black26,
                              blurRadius: 3.0,
                              offset: new Offset(0.0, 2.0),
                            )
                          ],
                        ),
                        child: Column(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(top: 15.0),
                              width: 130.0,
                              height: 130.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                boxShadow: [BoxShadow(
                                  color: Colors.grey,
                                  offset: new Offset(0.0, 4.0),
                                  blurRadius: 10.0,
                                  )
                                ],
                              ),
                              child: ClipOval(
                                child: Image(
                                  image: AssetImage('assets/images/cover4.png'),
                                  width: 130.0,
                                  height: 130.0,
                                  fit: BoxFit.cover
                                ),
                              )
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 15.0),
                              child: Text('Genero ' + (counter++).toString()),
                            )
                          ],
                        )
                      ),
                      Container(
                        width: (divWidth / 2) - 2,
                        height: 200.0,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            new BoxShadow(
                              color: Colors.black26,
                              blurRadius: 3.0,
                              offset: new Offset(0.0, 2.0),
                            )
                          ],
                        ),
                        child: Column(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(top: 15.0),
                              width: 130.0,
                              height: 130.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                boxShadow: [BoxShadow(
                                  color: Colors.grey,
                                  offset: new Offset(0.0, 4.0),
                                  blurRadius: 10.0,
                                  )
                                ],
                              ),
                              child: ClipOval(
                                child: Image(
                                  image: AssetImage('assets/images/cover3.jpg'),
                                  width: 130.0,
                                  height: 130.0,
                                  fit: BoxFit.cover
                                ),
                              )
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 15.0),
                              child: Text('Genero ' + (counter++).toString()),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
        ));
    }

    return Scaffold(
      drawer: Drawer(
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Container(
              height: divHeight,
              decoration: BoxDecoration(
                color: Colors.white,
                image: DecorationImage(
                  image: AssetImage('assets/images/backmenu_drawer.png'),
                  fit: BoxFit.fitWidth,
                  alignment: Alignment(0, -1)
                )
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 15.0),
                    child: Row(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 10.0, top: 15.0),
                          alignment: AlignmentDirectional(-1, -1),
                          width: 80.0,
                          height: 80.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Colors.grey,
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: NetworkImage('http://i.pravatar.cc/300'),
                              width: 80.0,
                              height: 80.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 15.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'JHOSIMAR MAMANI',
                                style: TextStyle(
                                  color: Color(0xFFF9F9F9),
                                ),
                              ),
                              Text(
                                'elratonmaton@gmail.com',
                                style: TextStyle(
                                  color: Color(0xFFF9F9F9)
                                ),
                              ),
                            ],
                          )
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 15.0,top: 10.0),
                    child: Row(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 10.0, top: 15.0, right: 20.0),
                          alignment: AlignmentDirectional(-1, -1),
                          width: 50.0,
                          height: 50.0,
                          decoration: BoxDecoration(
                            color: Color(0xFFb597ef),
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Color(0xFFc3a6f9),
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/pulse3.png'),
                              width: 50.0,
                              height: 50.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Text(
                          'Pedido de canciones',
                          style: TextStyle(
                            color: Color(0xFFF9F9F9),
                            fontWeight: FontWeight.bold
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 15.0,top: 10.0),
                    child: Row(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 10.0, top: 15.0, right: 20.0),
                          alignment: AlignmentDirectional(-1, -1),
                          width: 50.0,
                          height: 50.0,
                          decoration: BoxDecoration(
                            color: Color(0xFFb597ef),
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Color(0xFFe9e9e9),
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/equalizer.png'),
                              width: 50.0,
                              height: 50.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Text(
                          'Ecualizador',
                          style: TextStyle(
                            color: Color(0xFFA3B4FD),
                            fontWeight: FontWeight.bold
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 15.0,top: 10.0),
                    child: Row(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 10.0, top: 15.0, right: 20.0),
                          alignment: AlignmentDirectional(-1, -1),
                          width: 50.0,
                          height: 50.0,
                          decoration: BoxDecoration(
                            color: Color(0xFFb597ef),
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Color(0xFFe9e9e9),
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/paint.png'),
                              width: 50.0,
                              height: 50.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Text(
                          'Temas',
                          style: TextStyle(
                            color: Color(0xFFA3B4FD),
                            fontWeight: FontWeight.bold
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 15.0,top: 10.0),
                    child: Row(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 10.0, top: 15.0, right: 20.0),
                          alignment: AlignmentDirectional(-1, -1),
                          width: 50.0,
                          height: 50.0,
                          decoration: BoxDecoration(
                            color: Color(0xFFb597ef),
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(
                              color: Color(0xFFe9e9e9),
                              offset: new Offset(0.0, 4.0),
                              blurRadius: 10.0,
                              )
                            ],
                          ),
                          child: ClipOval(
                            child: Image(
                              image: AssetImage('assets/images/share.png'),
                              width: 50.0,
                              height: 50.0,
                              fit: BoxFit.cover
                            ),
                          )
                        ),
                        Text(
                          'Comparte la aplicación',
                          style: TextStyle(
                            color: Color(0xFFA3B4FD),
                            fontWeight: FontWeight.bold
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      body: Builder(
        builder: (BuildContext context) {
          return Container(
            
            child: Column(
              children: <Widget>[
                Container(
                  height: divHeight/2*0.3,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    image: DecorationImage(
                      image: AssetImage('assets/images/background-head_1.png'),
                      fit: BoxFit.cover
                    )
                  ),
                  child: Container(
                    padding: EdgeInsets.only(top: 8.0, left: 20.0, right: 20.0, bottom: 0.0),
                    child: Row(
                      children: <Widget>[
                        IconButton(
                          icon: Icon(
                            IconData(0xe3c7, fontFamily: 'MaterialIcons'),
                            size: 30.0,
                            color: Colors.white,
                          ),
                          tooltip: 'menu',
                          onPressed: () { Scaffold.of(context).openDrawer(); },
                        ),
                        Spacer(),
                        Text(
                          widget.title,
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                        Spacer(),
                        IconButton(
                          icon: Icon(
                            IconData(0xe8b6, fontFamily: 'MaterialIcons'),
                            size: 30.0,
                            color: Colors.white,
                          ),
                          tooltip: 'search',
                          onPressed: () { },
                        ),
                      ],
                    ),
                  ),
                ),

                Container(
                  height: divHeight - (divHeight/2*0.3),
                  child: ListView(
                    padding: EdgeInsets.only(top: 0.0),
                    shrinkWrap: true,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: children,
                      )
                    ],
                  ),
                ),
              ],
            ),
          );
        }
      )
    );
  }
}
