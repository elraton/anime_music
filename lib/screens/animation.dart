import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import './login.dart';
import 'dart:async';

class AnimationScreen extends StatefulWidget {
  AnimationScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _PageState createState() => _PageState();
}

class _PageState extends State<AnimationScreen> {
  _PageState() {
    Future.delayed(const Duration(milliseconds: 3000), () {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => LoginScreen(title: 'Login')),
      );
    });
  }
  
  

  void _incrementCounter() {
    setState(() { });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container (
              margin: const EdgeInsets.only(bottom: 15.0),
              child: 
                Image.asset(
                  'assets/images/logo_anime.png'
                ),
            ),
            CupertinoActivityIndicator()
          ],
        ),
      ),
    );
  }
}
