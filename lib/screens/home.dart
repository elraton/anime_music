import 'package:flutter/material.dart';
import './auxlib.dart';
import './drawer.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _PageState createState() => _PageState();
}

class _PageState extends State<HomeScreen> {
  int _counter = 0;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    var divHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      drawer: Drawer(
        child: CustomDrawer()
      ),
      body: Builder(
        builder: (BuildContext context) {
          return Container(
            
            child: Column(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    image: DecorationImage(
                      image: AssetImage('assets/images/background-head_1.png'),
                      fit: BoxFit.cover
                    )
                  ),
                  child: Padding(
                    padding: EdgeInsets.only(top: 8.0, left: 20.0, right: 20.0),
                    child: Row(
                      children: <Widget>[
                        IconButton(
                          icon: Icon(
                            IconData(0xe3c7, fontFamily: 'MaterialIcons'),
                            size: 30.0,
                            color: Colors.white,
                          ),
                          tooltip: 'menu',
                          onPressed: () { Scaffold.of(context).openDrawer(); },
                        ),
                        Spacer(),
                        Text(
                          widget.title,
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                        Spacer(),
                        IconButton(
                          icon: Icon(
                            IconData(0xe8b6, fontFamily: 'MaterialIcons'),
                            size: 30.0,
                            color: Colors.white,
                          ),
                          tooltip: 'search',
                          onPressed: () { },
                        ),
                      ],
                    ),
                  ),
                  height: divHeight/2*0.3,
                ),

                Container(
                  padding: EdgeInsets.only(left: 20.0, right: 20.0),
                  height: divHeight - (divHeight/2*0.3),
                  child: ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      AuxScreen()
                    ],
                  ),
                ),
              ],
            ),
          );
        }
      )
    );
  }
}
